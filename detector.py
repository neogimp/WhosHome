#!/usr/bin/python
import sys
from datetime import datetime
from shutil import copyfile
from scapy.all import srp,Ether,ARP,conf

#Imports

try:
    interface = 'wlp4s0' # Get interface to scan
    ips = '192.168.177.0/24'

except KeyboardIntertupt:
    print "\n[*] User Requested Shutdown"
    print "[*] Quitting"
    sys.exit(1)

# lets clear the img
copyfile('off.png','src/img/jesse.png' )
copyfile('off.png','src/img/dez.png' )
copyfile('off.png','src/img/patti.png' )
copyfile('off.png','src/img/freddie.png' )



print "\n[*] Scanning... " # Initiate Scanning
start_time =datetime.now() # start the clock


conf.verb = 0 #Actually start scanning
ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst = ips), timeout = 1,iface=interface,inter=0.05)


print "MAC - IP\n" 
for snd,rcv in ans:

    if rcv.hwsrc == "ac:37:43:a9:ad:69":
        print "Jesse Is Home"
        copyfile('on.png', 'src/img/jesse.png')
    elif rcv.hwsrc == "00:b3:62:33:1e:61":
        print "Dez is home"
        copyfile('on.png', 'src/img/dez.png')
    elif rcv.hwsrc == "dc:2b:2a;42:2c:f1":
        print "Patti is home"
        copyfile('on.png', 'src/img/patti.png')
    elif rcv.hwsrc == "d0:c5:f3:67:6d:f1":
        print "Freddie is home"
        copyfile('on.png', 'src/img/freddie.png')
    else:
        print rcv.sprintf(r"%Ether.src% - %ARP.psrc%")

stop_time = datetime.now()
total_time = stop_time - start_time
print "\n[*] Scan Complete!"
print ("[*] Scan Duration: %s" %(total_time))


